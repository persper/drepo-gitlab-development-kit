# GitLab Development Kit CLI parser / executor
#
# This file is loaded by the 'dgdk' command in the gem. This file is NOT
# part of the gitlab-development-kit gem so that we can iterate faster.

require_relative 'dgdk/env'

module DGDK
  PROGNAME = 'dgdk'.freeze
  MAKE = RUBY_PLATFORM =~ /bsd/ ? 'gmake' : 'make'

  # This function is called from bin/dgdk. It must return true/false or
  # an exit code.
  def self.main
    if !install_root_ok? && ARGV.first != 'reconfigure'
      puts <<-EOS.gsub(/^\s+\|/, '')
        |According to #{ROOT_CHECK_FILE} this gitlab-development-kit
        |installation was moved. Run 'dgdk reconfigure' to update hard-coded
        |paths.
      EOS
      return false
    end

    pg_port_file = File.join($dgdk_root, 'postgresql_port')
    pg_port = File.exist?(pg_port_file) ? File.read(pg_port_file) : 5432

    case ARGV.shift
    when 'run'
      exec('./run', *ARGV, chdir: $dgdk_root)
    when 'install'
      exec(MAKE, *ARGV, "ruby_version=#{RUBY_VERSION}", chdir: $dgdk_root)
    when 'update'
      # Otherwise we would miss it and end up in a weird state.
      puts "\n> Running `make self-update` separately in case the Makefile is updated..\n\n"
      system(MAKE, 'self-update', chdir: $dgdk_root)

      puts "\n> Running `make self-update update`..\n\n"
      exec(MAKE, "ruby_version=#{RUBY_VERSION}", 'self-update', 'update', chdir: $dgdk_root)
    when 'diff-config'
      require_relative './config_diff.rb'

      files = %w[
        gitlab/config/gitlab.yml
        gitlab/config/database.yml
        gitlab/config/unicorn.rb
        gitlab/config/resque.yml
        gitlab-shell/config.yml
        gitlab-shell/.gitlab_shell_secret
        redis/redis.conf
        .ruby-version
        Procfile
        gitlab-workhorse/config.toml
        gitaly/config.toml
        nginx/conf/nginx.conf
      ]

      file_diffs = files.map do |file|
        ConfigDiff.new(file)
      end

      file_diffs.each do |diff|
        $stderr.puts diff.make_output
      end

      file_diffs.each do |diff|
        puts diff.output unless diff.output == ""
      end

      true
    when 'reconfigure'
      remember!($dgdk_root)
      exec(MAKE, "ruby_version=#{RUBY_VERSION}", 'clean-config', 'unlock-dependency-installers', 'postgresql-sensible-defaults', 'all', chdir: $dgdk_root)
    when 'psql'
      exec('psql', '-h', File.join($dgdk_root, 'postgresql'), '-p', pg_port.to_s, *ARGV, chdir: $dgdk_root)
    when 'redis-cli'
      exec('redis-cli', '-s', File.join($dgdk_root, 'redis/redis.socket'), *ARGV, chdir: $dgdk_root)
    when 'env'
      DGDK::Env.exec(ARGV)
    when 'help'
      puts File.read(File.join($dgdk_root, 'HELP'))
      true
    else
      puts "Usage: #{PROGNAME} run|init|install|update|reconfigure|psql|redis-cli|diff-config|version|help [ARGS...]"
      false
    end
  end

  def self.install_root_ok?
    expected_root = File.read(File.join($dgdk_root, ROOT_CHECK_FILE)).chomp
    File.realpath(expected_root) == File.realpath($dgdk_root)
  rescue => ex
    warn ex
    false
  end
end
