# coding: utf-8

$LOAD_PATH.unshift(File.expand_path('../lib', __FILE__))
require 'drepo_gitlab_development_kit'

Gem::Specification.new do |spec|
  spec.name          = "drepo-gitlab-development-kit"
  spec.version       = DGDK::GEM_VERSION
  spec.authors       = ["Jacob Vosmaer", "B1nj0y"]
  spec.email         = ["jacob@gitlab.com", "huibin@meri.co"]

  spec.summary       = %q(A specified CLI for GitLab Development Kit enhanced by Drepo)
  spec.description   = %q(A specified CLI for GitLab Development Kit enhanced by Drepo.)
  spec.homepage      = "https://gitlab.com/persper/drepo-gitlab-development-kit"
  spec.license       = "MIT"
  spec.files         = ['lib/drepo_gitlab_development_kit.rb']
  spec.executables   = ['dgdk']
end
