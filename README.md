# Drepo GitLab Development Kit

DGDK forded from GDK to support versioned GitLab deployment. Besides most of common usages of GDK,
some features changed need to note as below:

* The original command `gdk` is crowned as **dgdk**, some usful commands: `dgdk init`, `dgdk install`, `dgdk run`, more see `dgdk help`
* For `dgdk install`:
  1. The default `gitlab_repo`(can be short as `repo`) is from *https://gitlab.com/persper/gitlab-ce*
  2. The default `version` is `master`, the version must start with a `v` when you want a specific version, e.g *dgkg install repo="SOME_REPO" version=v11.5.0*

## Installation

We didn't release the gem to the RubyGems, so you can download from [here](./gem/drepo-gitlab-development-kit-0.2.5.gem), then run `gem i drepo-gitlab-development-kit-X.Y.Z.gem` (change the X.Y.Z as it is) to install it to your current ruby environment.


# GitLab Development Kit

Configure and manage a [GitLab](https://about.gitlab.com) development
environment.

Read on for installation instructions or skip to
[doc/howto](doc/howto/README.md) for usage documentation.

## Overview

GitLab Development Kit (GDK) provides a collection of scripts and
other resources to install and manage a GitLab installation for
development purposes. The source code of GitLab is spread over
multiple repositories and it requires Ruby, Go, Postgres/MySQL, Redis
and more to run. GDK helps you install and configure all these
different components, and start/stop them when you work on GitLab.

## Contributing to GitLab Development Kit

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md)
for more details.

## Getting started

The preferred way to use GitLab Development Kit is to install Ruby and
dependencies on your 'native' OS. We strongly recommend the native install
since it is much faster than a virtualized one. Due to heavy IO operations a
virtualized installation will be much slower running the app and the tests.

To do a native install:

1. [Prepare your computer](doc/prepare.md)
2. [Set-up GDK](doc/set-up-gdk.md)

If you want to use Windows, you can try [our instructions for Windows Subsystem for Linux](doc/prepare.md#experimental-windows-10-using-the-wsl-windows-subsystem-for-linux).

Or if you want to use a slower virtualized installation with [Vagrant](https://www.vagrantup.com/),
please see the [instructions for using Vagrant with VirtualBox or Docker](doc/vagrant.md).

You can also install GDK on [Minikube](https://github.com/kubernetes/minikube),
see [kubernetes docs](doc/kubernetes.md).

After installation [learn how to use GDK](doc/howto/README.md)

If you have an old installation [update your existing GDK installation](doc/update-gdk.md)

## Design goals

- Get the user started, do not try to take care of everything
- Run everything as your 'desktop' user on your development machine
- GitLab Development Kit itself does not run `sudo` commands
- It is OK to leave some things to the user (e.g. installing Ruby)

## Differences with production

- gitlab-workhorse does not serve static files
- C compiler needed to run `bundle install` (not needed with Omnibus)
- GitLab can rewrite its program code and configuration data (read-only with
  Omnibus)
- 'Assets' (Javascript/CSS files) are generated on the fly (pre-compiled at
  build time with Omnibus)
- Gems (libraries) for development and functional testing get installed and
  loaded
- No unified configuration management for GitLab and gitlab-shell
  (handled by Omnibus)
- No privilege separation between Ruby, Postgres and Redis
- No easy upgrades
- Need to download and compile new gems ('bundle install') on each upgrade

## License

The GitLab Development Kit is distributed under the MIT license,
see the LICENSE file.
